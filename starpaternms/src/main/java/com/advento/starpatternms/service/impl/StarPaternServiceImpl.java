package com.advento.starpatternms.service.impl;

import org.springframework.stereotype.Service;

import com.advento.starpatternms.service.StarPatternService;

@Service(value = "starPaternServiceImpl")
public class StarPaternServiceImpl implements StarPatternService {

	@Override
	public String starPatern(String source, int length) {

		String patern = "";
		if (length > 0) {
			for (int i = 0; i < length; i++) {

				for (int j = 0; j <= i; j++) {
					patern = patern + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					patern = patern + "\n";
				} else {
					patern = patern + "<br />";
				}

			}

		}
		return patern;
	}

	@Override
	public String leftAnglePatern(String source, int length) {

		String leftPatern = "";
		if (length > 0) {
			for (int i = 1; i <= length; i++) {

				for (int j = length; j >= i; j--) {
					leftPatern = leftPatern + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					leftPatern = leftPatern + "\n";
				} else {
					leftPatern = leftPatern + "<br />";
				}

			}

		}
		return leftPatern;
	}

	@Override
	public String rightPascalTrianglePattern(String source, int length) {

		String pattern = "";
		if (length > 0) {
			for (int i = 1; i <= length; i++) {
				for (int j = 1; j <= i; j++) {
					pattern = pattern + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					pattern = pattern + "\n";
				} else {
					pattern = pattern + "<br />";
				}
			}
			for (int i = length - 1; i >= 1; i--) {
				for (int j = 1; j <= i; j++) {
					pattern = pattern + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					pattern = pattern + "\n";
				} else {
					pattern = pattern + "<br />";
				}

			}

		}
		return pattern;
	}
	
	@Override
	public String pyramidPattern(String source, int length) {

		String pattern = "";
		if (length > 0) {
			for (int i = 1; i <= length; i++) {
				for (int j = length - i; j >= 1; j--) {
						pattern = pattern + " ";
				}
				for (int j = 1; j <= i; j++) {
					pattern = pattern + "* ";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					pattern = pattern + "\n";
				} else {
					pattern = pattern + "<br />";
				}
			}

		}
		return pattern;
	}

	@Override
	public String reversePyramidPattern(String source, int length) {

		String pattern = "";
		if (length > 0) {
			 for (int i=5; i>=1; --i) 
	            { 
		        	for(int s = 1; s <= 5 - i; s++) {
		        		pattern = pattern + "  ";
		              }
		        	for (int j=i; j<=2 * i; j++)
		            {
		        		pattern = pattern + " ";
		            }
		            for (int j=0; j<i - 1; j++ ) 
		            { 
		            	pattern = pattern + "* "; 
		            } 
		            if (source != null && source.equalsIgnoreCase("postman")) {
						pattern = pattern + "\n";
					} else {
						pattern = pattern + "<br />";
					} 
	            }

		}
		return pattern;
	}
	
	@Override
	public String diamondPattern(String source, int length) {

		String pattern = "";
		if (length > 0) {
			int i, j, s = 4;
			for (j = 1; j <= 5; j++) {
				for (i = 1; i <= s; i++) {
					pattern = pattern + " ";
				}
				s--;
				for (i = 1; i <= 2 * j - 1; i++) {
					pattern = pattern + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					pattern = pattern + "\n";
				} else {
					pattern = pattern + "<br />";
				} 
			}
			s = 1;
			for (j = 1; j <= 4; j++) {
				for (i = 1; i <= s; i++) {
					pattern = pattern + " ";
				}
				s++;
				for (i = 1; i <= 2 * (5 - j) - 1; i++) {
					pattern = pattern + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					pattern = pattern + "\n";
				} else {
					pattern = pattern + "<br />";
				} 
			}

		}
		return pattern;
	}
}
