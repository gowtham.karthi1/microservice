package com.advento.starpatternms.service;

public interface StarPatternService {

	public String starPatern(String source, int length);
	
	public String leftAnglePatern(String source, int length);
	
	public String rightPascalTrianglePattern(String source, int length);
	
	public String pyramidPattern(String source, int length);
	
	public String reversePyramidPattern(String source, int length);
	
	public String diamondPattern(String source, int length);
}
