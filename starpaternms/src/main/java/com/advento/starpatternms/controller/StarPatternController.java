package com.advento.starpatternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.advento.starpatternms.service.StarPatternService;

@RestController
public class StarPatternController {

	@Autowired
	@Qualifier(value = "starPaternServiceImpl")
	private StarPatternService starPaternService;
		
	@GetMapping(value = "/patern")
	public ResponseEntity<String> tarPatern(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
	String patern = starPaternService.starPatern(source, length);
	ResponseEntity<String> responseEntity = new ResponseEntity<String>(patern, HttpStatus.OK);
	return responseEntity;
	}
	
	@GetMapping(value = "/leftpatern")
	public ResponseEntity<String> leftAnglePatern(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
	String leftPatern = starPaternService.leftAnglePatern(source, length);
	ResponseEntity<String> responseEntity = new ResponseEntity<String>(leftPatern, HttpStatus.OK);
	return responseEntity;
	}

	@GetMapping(value = "/rightpascaltriangle")
	public ResponseEntity<String> rightPascalTrianglePattern(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
	String pattern = starPaternService.rightPascalTrianglePattern(source, length);
	ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
	return responseEntity;
	}
	
	@GetMapping(value = "/pyramidpattern")
	public ResponseEntity<String> pyramidPattern(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
	String pattern = starPaternService.pyramidPattern(source, length);
	ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
	return responseEntity;
	}
	
	@GetMapping(value = "/reversepyramidpattern")
	public ResponseEntity<String> reversePyramidPattern(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
	String pattern = starPaternService.reversePyramidPattern(source, length);
	ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
	return responseEntity;
	}
	
	@GetMapping(value = "/diamondpattern")
	public ResponseEntity<String> diamondPattern(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
	String pattern = starPaternService.diamondPattern(source, length);
	ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
	return responseEntity;
	}
}

