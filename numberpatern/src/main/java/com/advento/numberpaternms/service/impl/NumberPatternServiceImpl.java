package com.advento.numberpaternms.service.impl;

import org.springframework.stereotype.Service;

import com.advento.numberpaternms.service.NumberPatternService;

@Service(value = "numberPatternServiceImpl")
public class NumberPatternServiceImpl implements NumberPatternService{

	@Override
	public String numberPattern1(String source, int length) {
		String patern = "";
		if (length > 0) {
			 for(int i=1; i<=length; i++) { 
	             
		            for(int j=1; j<=i; j++) {   
		            	patern = patern + i + " "; 
		                 
		            } 
		            if (source != null && source.equalsIgnoreCase("postman")) {
						patern = patern + "\n";
					} else {
						patern = patern + "<br />";
					} 
		        } 

		}
		return patern;
	}

	@Override
	public String numberPattern2(String source, int length) {
		String patern = "";
		if (length > 0) {
			 for(int i=1; i<=length; i++) { 
	             
		            for(int j=1; j<=i; j++) {   
		            	patern = patern + j + " "; 
		                 
		            } 
		            if (source != null && source.equalsIgnoreCase("postman")) {
						patern = patern + "\n";
					} else {
						patern = patern + "<br />";
					} 
		        } 

		}
		return patern;
	}

	@Override
	public String numberPattern3(String source, int length) {
		String patern = "";
		if (length > 0) {
			 int k=1;
		        for(int i=1; i<=6; i++) { 
		             
		            for(int j=1; j<=i; j++) {   
		            	patern = patern + k++ + " "; 
		                
		            } 
		            if (source != null && source.equalsIgnoreCase("postman")) {
						patern = patern + "\n";
					} else {
						patern = patern + "<br />";
					} 
		            
		        }

		}
		return patern;
	}

}
