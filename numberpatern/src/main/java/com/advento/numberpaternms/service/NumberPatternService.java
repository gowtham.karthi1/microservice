package com.advento.numberpaternms.service;

public interface NumberPatternService {

	public String numberPattern1(String source, int length);
	
	public String numberPattern2(String source, int length);
	
	public String numberPattern3(String source, int length);
}
