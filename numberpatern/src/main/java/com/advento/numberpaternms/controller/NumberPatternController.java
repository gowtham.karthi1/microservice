package com.advento.numberpaternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.advento.numberpaternms.service.NumberPatternService;

@RestController
public class NumberPatternController {

	@Autowired
	@Qualifier(value = "numberPatternServiceImpl")
	private NumberPatternService numberPatternService;

	@GetMapping(value = "/numberpatern1")
	public ResponseEntity<String> numberPatern1(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
		String patern = numberPatternService.numberPattern1(source, length);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(patern, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/numberpatern2")
	public ResponseEntity<String> numberPatern2(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
		String patern = numberPatternService.numberPattern2(source, length);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(patern, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/numberpatern3")
	public ResponseEntity<String> numberPatern3(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
		String patern = numberPatternService.numberPattern3(source, length);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(patern, HttpStatus.OK);
		return responseEntity;
	}
}
