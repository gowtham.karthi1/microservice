package com.advento.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.advento.employee.dto.EmployeeDTO;
import com.advento.employee.service.EmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	@Qualifier(value = "employeeServiceImpl")
	private EmployeeService employeeService;
	
	@GetMapping(value = "/employees")
	public ResponseEntity<EmployeeDTO[]> getEmployees() {
		
		EmployeeDTO[] employeeDTOs = employeeService.getEmployees();
		ResponseEntity<EmployeeDTO[]> responseEntity = new ResponseEntity<EmployeeDTO[]>(employeeDTOs, HttpStatus.OK);
		return responseEntity;
	}
}
