package com.advento.employee.service.impl;

import org.springframework.stereotype.Service;

import com.advento.employee.dto.EmployeeDTO;
import com.advento.employee.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	@Override
	public EmployeeDTO[] getEmployees() {
		EmployeeDTO employeeDTO1 = createEmployeeObj(1, "Gowtham", 25, "gowtham@gmail.com");
		EmployeeDTO employeeDTO2 = createEmployeeObj(2, "Karthi", 24, "karthi@gmail.com");
		EmployeeDTO employeeDTO3 = createEmployeeObj(3, "Arun", 23, "arun@gmail.com");
		EmployeeDTO employeeDTO4 = createEmployeeObj(4, "Goklu", 23, "gokul@gmail.com");
		EmployeeDTO employeeDTO5 = createEmployeeObj(5, "Subash", 22, "subash@gmail.com");
		EmployeeDTO employeeDTO6 = createEmployeeObj(6, "Guru", 34, "guru@gmail.com");
		EmployeeDTO employeeDTO7 = createEmployeeObj(7, "Karthikeyan", 55, "karthikeyan@gmail.com");
		
		EmployeeDTO[] employeeDTOs = new EmployeeDTO[7];
		employeeDTOs[0] = employeeDTO1;
		employeeDTOs[1] = employeeDTO2;
		employeeDTOs[2] = employeeDTO3;
		employeeDTOs[3] = employeeDTO4;
		employeeDTOs[4] = employeeDTO5;
		employeeDTOs[5] = employeeDTO6;
		employeeDTOs[6] = employeeDTO7;
		
		return employeeDTOs;
	}

	private EmployeeDTO createEmployeeObj(int id, String name, int age, String email) {

		EmployeeDTO employeeDTO = new EmployeeDTO();
		employeeDTO.setId(id);
		employeeDTO.setName(name);
		employeeDTO.setAge(age);
		employeeDTO.setEmail(email);
		return employeeDTO;
	}
}
