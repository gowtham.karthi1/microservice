package com.advento.printnumbers.service;

public interface PrintNumberService {

	public String oneToHundred(String source);

	public String evenNumber(String source);

	public String oddNumber(String source);

	public String evenNumBtwGivenNum(String source, int firstNum, int lastNum);

	public String oddNumBtwGivenNum(String source, int firstNum, int lastNum);

	public String tables(String source, int num);
		
	
}
