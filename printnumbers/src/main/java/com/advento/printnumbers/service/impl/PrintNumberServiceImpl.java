package com.advento.printnumbers.service.impl;

import org.springframework.stereotype.Service;

import com.advento.printnumbers.service.PrintNumberService;

@Service(value = "printNumberServiceImpl")
public class PrintNumberServiceImpl implements PrintNumberService {

	@Override
	public String oneToHundred(String source) {
		String p = "";
			for (int i = 1; i <= 100; i++) {
				p = p + i + " ";
			}

		return p;
	}

	@Override
	public String evenNumber(String source) {
		String p = "";
		for (int i = 1; i <= 100; i++) {
			if (i % 2 == 0) {
				p = p + i + " ";
			}
		}

	return p;
	}

	@Override
	public String oddNumber(String source) {
		String p = "";
		for (int i = 1; i <= 100; i++) {
			if (i % 2 == 1) {
				p = p + i + " ";
			}
		}

	return p;
	}

	@Override
	public String evenNumBtwGivenNum(String source, int firstNum, int lastNum) {
		String p = "";
		for (int i = firstNum; i <= lastNum; i++) {
			if (i % 2 == 0) {
				p = p + i + " ";
			}
		}

	return p;
	}

	@Override
	public String oddNumBtwGivenNum(String source, int firstNum, int lastNum) {
		String p = "";
		for (int i = firstNum; i <= lastNum; i++) {
			if (i % 2 == 1) {
				p = p + i + " ";
			}
		}

	return p;
	}

	@Override
	public String tables(String source, int num) {
		String p = "";
		for (int i = 1; i <= 20; i++) {
				p = p + i * num + " ";
		}

	return p;
	}
}
