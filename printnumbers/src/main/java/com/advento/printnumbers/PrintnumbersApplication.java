package com.advento.printnumbers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrintnumbersApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrintnumbersApplication.class, args);
	}

}
