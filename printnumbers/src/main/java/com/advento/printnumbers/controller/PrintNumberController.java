package com.advento.printnumbers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.advento.printnumbers.service.PrintNumberService;

@RestController
public class PrintNumberController {

	@Autowired
	@Qualifier(value = "printNumberServiceImpl")
	private PrintNumberService printNumberService;

	@GetMapping(value = "/onetohundred")
	public ResponseEntity<String> oneToHundred(
			@RequestParam(value = "source", required = false) String source) {
		String printNum = printNumberService.oneToHundred(source);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(printNum, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/evennumber")
	public ResponseEntity<String> evenNumber(
			@RequestParam(value = "source", required = false) String source) {
		String printNum = printNumberService.evenNumber(source);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(printNum, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/oddnumber")
	public ResponseEntity<String> oddNumber(
			@RequestParam(value = "source", required = false) String source) {
		String printNum = printNumberService.oddNumber(source);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(printNum, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/evennumbtwgivennum")
	public ResponseEntity<String> evenNumBtwGivenNum(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "firstNum") int firstNum,
			@RequestParam(value = "lastNum") int lastNum){
		String printNum = printNumberService.evenNumBtwGivenNum(source, firstNum, lastNum);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(printNum, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/oddnumbtwgivennum")
	public ResponseEntity<String> oddNumBtwGivenNum(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "firstNum") int firstNum,
			@RequestParam(value = "lastNum") int lastNum){
		String printNum = printNumberService.oddNumBtwGivenNum(source, firstNum, lastNum);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(printNum, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/tables")
	public ResponseEntity<String> tables(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "num") int num){
		String printNum = printNumberService.tables(source, num);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(printNum, HttpStatus.OK);
		return responseEntity;
	}
}
